# translation of kcm_autostart.po to Punjabi
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Amanpreet Singh <aalam@users.sf.net>, 2008.
# SPDX-FileCopyrightText: 2008, 2010, 2020, 2021, 2023, 2024 A S Alam <aalam@users.sf.net>
# Amanpreet Singh Alam <aalam@users.sf.net>, 2009, 2012, 2013.
msgid ""
msgstr ""
"Project-Id-Version: kcm_autostart\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-18 00:39+0000\n"
"PO-Revision-Date: 2024-01-03 14:00-0600\n"
"Last-Translator: A S Alam <aalam@punlinux.org>\n"
"Language-Team: Punjabi <kde-i18n-doc@kde.org>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.08.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: autostartmodel.cpp:376
#, kde-format
msgid "\"%1\" is not an absolute url."
msgstr "\"%1\" ਅਸਲ url ਨਹੀਂ ਹੈ।"

#: autostartmodel.cpp:379
#, kde-format
msgid "\"%1\" does not exist."
msgstr "\"%1 ਮੌਜੂਦ ਨਹੀਂ ਹੈ।"

#: autostartmodel.cpp:382
#, kde-format
msgid "\"%1\" is not a file."
msgstr "\"%1\" ਫਾਇਲ ਨਹੀਂ ਹੈ।"

#: autostartmodel.cpp:385
#, kde-format
msgid "\"%1\" is not readable."
msgstr "\"%1\" ਪੜ੍ਹਨਯੋਗ ਨਹੀਂ।"

#: ui/entry.qml:52
#, kde-format
msgctxt ""
"@label The name of a Systemd unit for an app or script that will autostart"
msgid "Name:"
msgstr "ਨਾਂ:"

#: ui/entry.qml:58
#, kde-format
msgctxt ""
"@label The current status (e.g. active or inactive) of a Systemd unit for an "
"app or script that will autostart"
msgid "Status:"
msgstr "ਹਾਲਤ:"

#: ui/entry.qml:64
#, kde-format
msgctxt ""
"@label A date and time follows this text, making a sentence like 'Last "
"activated on: August 7th 11 PM 2023'"
msgid "Last activated on:"
msgstr "ਆਖਰੀ ਵਾਰ ਸਰਗਰਮੀ ਸੀ:"

#: ui/entry.qml:74
#, kde-format
msgctxt "@label Stop the Systemd unit for a running process"
msgid "Stop"
msgstr "ਰੋਕੋ"

#: ui/entry.qml:74
#, kde-format
msgctxt "@label Start the Systemd unit for a currently inactive process"
msgid "Start"
msgstr "ਸ਼ੁਰੂ"

#: ui/entry.qml:109
#, kde-format
msgid "Unable to load logs. Try refreshing."
msgstr "ਲਾਗ ਲੋਡ ਕਰਨ ਲਈ ਅਸਮਰ਼ਥ ਹੈ। ਤਾਜ਼ਾ ਕਰਨ ਦੀ ਕੋਸ਼ਿਸ਼ ਕਰੋ।"

#: ui/entry.qml:113
#, kde-format
msgctxt "@action:button Refresh entry logs when it failed to load"
msgid "Refresh"
msgstr "ਤਾਜ਼ਾ ਕਰੋ"

#: ui/main.qml:33
#, kde-format
msgid "Make Executable"
msgstr "ਚੱਲਣਯੋਗ ਬਣਾਓ"

#: ui/main.qml:53
#, kde-format
msgid "The file '%1' must be executable to run at logout."
msgstr "ਲਾਗ-ਆਉਟ ਸਮੇਂ ਚਲਾਉਣ ਲਈ '%1' ਫਾਇਲ ਚੱਲਣਯੋਗ ਚਾਹੀਦੀ ਹੈ।"

#: ui/main.qml:55
#, kde-format
msgid "The file '%1' must be executable to run at login."
msgstr "ਲਾਗ-ਇਨ ਸਮੇਂ ਚਲਾਉਣ ਲਈ '%1' ਫਾਇਲ ਚੱਲਣਯੋਗ ਚਾਹੀਦੀ ਹੈ।"

#: ui/main.qml:66
#, kde-format
msgctxt "@action:button"
msgid "Add…"
msgstr "…ਜੋੜੋ"

#: ui/main.qml:69
#, kde-format
msgctxt "@action:button"
msgid "Add Application…"
msgstr "…ਐਪਲੀਕੇਸ਼ਨ ਜੋੜੋ"

#: ui/main.qml:74
#, kde-format
msgctxt "@action:button"
msgid "Add Login Script…"
msgstr "…ਲਾਗਇਨ ਸਕ੍ਰਿਪਟ ਜੋੜੋ"

#: ui/main.qml:79
#, kde-format
msgctxt "@action:button"
msgid "Add Logout Script…"
msgstr "…ਲਾਗਆਉਟ ਸਕ੍ਰਿਪਟ ਜੋੜੋ"

#: ui/main.qml:114
#, kde-format
msgid ""
"%1 has not been autostarted yet. Details will be available after the system "
"is restarted."
msgstr ""

#: ui/main.qml:137
#, kde-format
msgctxt ""
"@label Entry hasn't been autostarted because system hasn't been restarted"
msgid "Not autostarted yet"
msgstr "ਹਾਲੇ ਕੋਈ ਆਟੋ-ਸਟਾਰਟ ਨਹੀਂ ਹੈ"

#: ui/main.qml:146
#, kde-format
msgctxt "@action:button"
msgid "See properties"
msgstr "ਵਿਸ਼ੇਸ਼ਤਾਵਾਂ ਵੇਖੋ"

#: ui/main.qml:157
#, kde-format
msgctxt "@action:button"
msgid "Remove entry"
msgstr "ਐੰਟਰੀ ਹਟਾਓ"

#: ui/main.qml:173
#, kde-format
msgid "Applications"
msgstr "ਐਪਲੀਕੇਸ਼ਨਾਂ"

#: ui/main.qml:176
#, kde-format
msgid "Login Scripts"
msgstr "ਲਾਗਇਨ ਸਕ੍ਰਿਪਟਾਂ"

#: ui/main.qml:179
#, kde-format
msgid "Pre-startup Scripts"
msgstr "ਪ੍ਰੀ-ਸਟਾਰਟਅੱਪ ਸਕ੍ਰਿਪਟਾਂ"

#: ui/main.qml:182
#, kde-format
msgid "Logout Scripts"
msgstr "ਲਾਗਆਉਟ ਸਕ੍ਰਿਪਟਾਂ"

#: ui/main.qml:191
#, kde-format
msgid "No user-specified autostart items"
msgstr ""

#: ui/main.qml:192
#, kde-kuit-format
msgctxt "@info 'some' refers to autostart items"
msgid "Click the <interface>Add…</interface> button to add some"
msgstr "ਕੁਝ ਜੋੜਨ ਲਈ <interface>…ਜੋੜੋ</interface> ਬਟਨ ਨੂੰ ਕਲਿੱਕ ਕਰੋ"

#: ui/main.qml:207
#, kde-format
msgid "Choose Login Script"
msgstr "ਲਾਗਇਨ ਸਕ੍ਰਿਪਟ ਚੁਣੋ"

#: ui/main.qml:227
#, kde-format
msgid "Choose Logout Script"
msgstr "ਲਾਗਆਉਟ ਸਕ੍ਰਿਪਟ ਚੁਣੋ"

#: unit.cpp:26
#, kde-format
msgctxt "@label Entry is running right now"
msgid "Running"
msgstr "ਚੱਲ ਰਿਹਾ ਹੈ"

#: unit.cpp:27
#, kde-format
msgctxt "@label Entry is not running right now (exited without error)"
msgid "Not running"
msgstr "ਨਹੀਂ ਚੱਲ ਰਿਹਾ ਹੈ"

#: unit.cpp:28
#, kde-format
msgctxt "@label Entry is being started"
msgid "Starting"
msgstr "ਸ਼ੁਰੂ ਕੀਤਾ ਜਾ ਰਿਹਾ ਹੈ"

#: unit.cpp:29
#, kde-format
msgctxt "@label Entry is being stopped"
msgid "Stopping"
msgstr "ਰੋਕਿਆ ਜਾ ਰਿਹਾ ਹੈ"

#: unit.cpp:30
#, kde-format
msgctxt "@label Entry has failed (exited with an error)"
msgid "Failed"
msgstr "ਅਸਫ਼ਲ ਹੈ"

#: unit.cpp:83
#, kde-format
msgid "Error occurred when receiving reply of GetAll call %1"
msgstr ""

#: unit.cpp:155
#, kde-format
msgid "Failed to open journal"
msgstr "ਜਰਨਲ ਨੂੰ ਖੋਲ੍ਹਣ ਲਈ ਫੇਲ੍ਹ ਹੈ"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ ੨੦੨੦"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "alam.yellow@gmail.com"

#~ msgid "Autostart"
#~ msgstr "ਆਟੋ-ਸਟਾਰਟ"

#~ msgid "Session Autostart Manager Control Panel Module"
#~ msgstr "ਸ਼ੈਸ਼ਨ ਆਟੋਸਟਾਰਟ ਮੈਨੇਜਰ ਕੰਟਰੋਲ ਪੈਨਲ ਮੋਡੀਊਲ"

#~ msgid "Copyright © 2006–2020 Autostart Manager team"
#~ msgstr "Copyright © 2006–2020 Autostart ਮੈਨੇਜਰ ਟੀਮ"

#~ msgid "Stephen Leaf"
#~ msgstr "Stephen Leaf"

#~ msgid "Montel Laurent"
#~ msgstr "Montel Laurent"

#~ msgid "Maintainer"
#~ msgstr "ਪਰਬੰਧਕ"

#~ msgid "Nicolas Fella"
#~ msgstr "ਨਿਕੋਲਸ ਫੇਲਾ"

#~ msgid "Add..."
#~ msgstr "...ਜੋੜੋ"

#~ msgid "Shell script path:"
#~ msgstr "ਸੈੱਲ ਸਕ੍ਰਿਪਟ ਪਾਥ:"

#~ msgid "Create as symlink"
#~ msgstr "ਸਿਮਲਿੰਕ ਵਾਂਗ ਬਣਾਓ"

#, fuzzy
#~| msgid "Autostart only in KDE"
#~ msgid "Autostart only in Plasma"
#~ msgstr "KDE ਵਿੱਚ ਹੀ ਆਟੋ-ਸਟਾਰਟ"

#~ msgid "Command"
#~ msgstr "ਕਮਾਂਡ"

#, fuzzy
#~| msgctxt ""
#~| "@title:column The name of the column that decides if the program is run "
#~| "on kde startup, on kde shutdown, etc"
#~| msgid "Run On"
#~ msgctxt ""
#~ "@title:column The name of the column that decides if the program is run "
#~ "on session startup, on session shutdown, etc"
#~ msgid "Run On"
#~ msgstr "ਚਲਾਓ"

#, fuzzy
#~| msgid "KDE Autostart Manager"
#~ msgid "Session Autostart Manager"
#~ msgstr "KDE ਆਟੋ-ਸਟਾਰਟ ਮੈਨੇਜਰ"

#~ msgctxt "The program will be run"
#~ msgid "Enabled"
#~ msgstr "ਯੋਗ ਹੈ"

#~ msgctxt "The program won't be run"
#~ msgid "Disabled"
#~ msgstr "ਆਯੋਗ ਹੈ"

#~ msgid "Desktop File"
#~ msgstr "ਡੈਸਕਟਾਪ ਫਾਈਲ"

#~ msgid "Script File"
#~ msgstr "ਸਕ੍ਰਿਪਟ ਫਾਈਲ"

#~ msgid "Add Program..."
#~ msgstr "ਪਰੋਗਰਾਮ ਸ਼ਾਮਲ..."

#~ msgid ""
#~ "Only files with “.sh” extensions are allowed for setting up the "
#~ "environment."
#~ msgstr "ਸੈਟਿੰਗ ਅੱਪ ਇੰਵਾਇਰਨਮੈਂਟ ਲਈ ਕੇਵਲ “.sh” ਇਕਸਟੈਨਸ਼ਨ ਵਾਲੀਆਂ ਫਾਈਲਾਂ ਹੀ ਪੜ੍ਹ ਸਕਦਾ ਹੈ।"

#~ msgid "Shutdown"
#~ msgstr "ਬੰਦ ਕਰੋ"
